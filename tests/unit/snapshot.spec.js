import SongItemVue from '@/components/SongItem.vue';
import { shallowMount, RouterLinkStub } from '@vue/test-utils';

describe('Snapshot SongItem', () => {
    test('renders correctly', () => {
        const song = {
            docID: 'abc',
            modified_name: 'test',
            display_name: 'test',
            comment_count: 5,
        };

        const wrapper = shallowMount(SongItemVue, {
            propsData: { song },
            global: {
                components: {
                    'router-link': RouterLinkStub,
                },
            },
        });
        expect(wrapper.element).toMatchSnapshot();
    });
});