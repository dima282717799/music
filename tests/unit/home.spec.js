import { shallowMount } from '@vue/test-utils';
import HomeViewVue from '@/views/HomeView.vue';
import SongItemVue from '@/components/SongItem.vue';

jest.mock('@/includes/firebase', () => {});

describe('Home.vue', () => {
    test('rendes list of songs', () => {
        const songs = [{}, {}, {}];

        HomeViewVue.methods.getSongs = () => false;

        const component = shallowMount(HomeViewVue, {
            data() {
                return {
                    songs,
                };
            },
            global: {
                mocks: {
                    $t: (message) => message,
                },
            },
        });
        const items = component.findAllComponents(SongItemVue);

        expect(items).toHaveLength(songs.length);

        items.forEach((wrapper, i) => {
            expect(wrapper.props().song).toStrictEqual(songs[i]);
        });
    });
});