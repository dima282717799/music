import { RouterLinkStub, shallowMount } from '@vue/test-utils';
import SongItemVue from '@/components/SongItem.vue';

describe('Router', () => {
    test('renders router link', () => {
        const song = {
            docID: 'abc',
        };
        const wrapper = shallowMount(SongItemVue, {
            propsData: { song },
            global: {
                components: {
                    'router-link': RouterLinkStub,
                },
            },
        });
        expect(wrapper.findComponent(RouterLinkStub).props().to).toEqual({ name: 'song', params: { id: song.docID } });
    });
});