module.exports = {
    root: true,
    env: {
        node: true,
        jest: true,
    },
    extends: [
        'plugin:vue/vue3-essential',
        '@vue/airbnb',
    ],
    parserOptions: {
        parser: '@babel/eslint-parser',
        requireConfigFile: false,
    },
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        indent: ['error', 4],
        'eol-last': ['error', 'never'],
        'vuejs-accessibility/label-has-for': [
            'error',
            {
                components: ['VLabel'],
                controlComponents: ['VInput'],
                required: {
                    some: ['nesting', 'id'],
                },
                allowChildren: false,
            },
        ],
        'max-len': ['error', { code: 200 }],
        'semi-spacing': ['error', { before: false, after: false }],
        'func-call-spacing': ['error', 'never'],
        'vue/multi-word-component-names': 0,
    },
    overrides: [{
        files: [
            '**/__tests__/*.{j,t}s?(x)',
            '**/tests/unit/**/*.spec.{j,t}s?(x)',
        ],
        rules: {
            'vue/multi-word-component-names': 0,
        },
    }],
};