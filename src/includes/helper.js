export default {
    formatTime(time) {
        const minustes = Math.floor(time / 60) || 0;
        const seconds = Math.round((time - minustes * 60) || 0);
        return `${minustes}:${seconds < 10 ? '0' : ''}${seconds}`;
    },
};