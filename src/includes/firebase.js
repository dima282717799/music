import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

const firebaseConfig = {

    apiKey: 'AIzaSyAb_DKUWoqu4VFXWyGZED17_BcHApA9ApI',

    authDomain: 'music-22d9e.firebaseapp.com',

    projectId: 'music-22d9e',

    storageBucket: 'music-22d9e.appspot.com',

    messagingSenderId: '87116573170',

    appId: '1:87116573170:web:a7799a60b70b2a7c22636b',

};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const db = firebase.firestore();
const storage = firebase.storage();

const usersCollection = db.collection('users');
const songsCollection = db.collection('songs');
const commentsCollection = db.collection('comments');

export {
    auth,
    db,
    usersCollection,
    songsCollection,
    commentsCollection,
    storage,

};