/* eslint-disable import/no-import-module-exports */
import camelCase from 'lodash/camelCase';

const requireModule = require.context('.', false, /\.js$/);

const modules = {};

// eslint-disable-next-line consistent-return
requireModule.keys().forEach((fileName) => {
    if (fileName === './index.js' || fileName === './dummy.js') {
        return 0;
    }

    const moduleConfig = requireModule(fileName);
    const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ''));
    modules[moduleName] = moduleConfig.defaule || moduleConfig;
    console.log(modules);
});

export default modules;