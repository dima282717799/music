/* eslint-disable func-call-spacing */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable indent */
import { createRouter, createWebHistory } from 'vue-router';
import store from '@/store';

const routes = [{
        name: 'home',
        path: '/',
        component: () =>
            import ('@/views/HomeView.vue'),
    },
    {
        name: 'about',
        path: '/about',
        component: () =>
            import ('@/views/AboutView.vue'),
    },
    {
        name: 'manage',
        path: '/manage-music',
        component: () =>
            import ('@/views/Manage.vue'),
        meta: {
            requiresAuth: true,
        },
        // beforeEach: (to, from, next) => {},
    },
    {
        path: '/manage',
        redirect: { name: 'manage' },
    },
    {
        name: 'song',
        path: '/song/:id',
        component: () =>
            import ('@/views/Song.vue'),
    },
    {
        path: '/:catchAll(.*)*',
        redirect: { name: 'home' },
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
    linkExactActiveClass: 'text-yellow-500',
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        next();
        return;
    }

    if (store.state.auth.userLoggedIn) {
        next();
    } else {
        next({ name: 'home' });
    }
});

export default router;